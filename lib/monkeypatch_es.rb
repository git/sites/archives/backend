# Monkeypatch the real-ES check
module Elasticsearch
  class Client
    def verify_with_version_or_header(version, headers)
      @verified = true
    end
  end
end

