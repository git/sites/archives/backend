module Ag
  module Threading
    module_function

    # Figures out the Message-Id of the parent message,
    # or returns nil if we asusme this message is not a reply
    def get_parent_message_id(mail)
      # No headers -> no parent message
      if mail.in_reply_to.nil? && mail.references.nil?
        return nil
      else
        irt_value = nil

        if mail.in_reply_to.is_a? Array
          irt_value = mail.in_reply_to.last
        elsif mail.in_reply_to.is_a? String
          irt_value = mail.in_reply_to

          # rubocop:disable Style/AsciiComments
          # Gnus/Emacs specialty du jour
          # => "<1075186049.4264.1.camel@TesterTop.tester.ca> (Olivier Crête's message of \"Tue, 27 Jan 2004 07:47:29 +0100\")"
          # rubocop:enable Style/AsciiComments
          if irt_value.start_with? '<'
            irt_value = irt_value[0..irt_value.rindex('>')] unless irt_value.end_with? '>'
            irt_value.gsub!(/(^<|>$)/, '')
          end
        elsif mail.in_reply_to.nil?
          # nothing to do
        elsif $options.debug
          $stderr.puts "In-Reply-To is a weird type: #{mail.message_id}"
        end

        ref_value = nil
        if mail.references.is_a? Array
          ref_value = mail.references.last
        elsif mail.references.is_a? String
          ref_value = mail.references
        elsif mail.references.nil?
          # nothing to do
        else
          $stderr.puts "References is a weird type: #{mail.message_id}" if $options.debug
        end

        return irt_value.to_s if irt_value == ref_value
        return ref_value.to_s if irt_value.nil?
        return irt_value.to_s if ref_value.nil?

        $stderr.puts "In-Reply-To and References disagree: #{mail.message_id}" if $options.debug
        # If in doubt, let In-Reply-To win
        return irt_value.to_s
      end

      $stderr.puts "Couldn't find a parent id for Message-Id: #{mail.message_id}" if $options.debug
      nil
    end

    def calc(list)
      number_of_root_threads = -1
      pass = 1
      loop do
        new_num = Ag::Storage.fix_threading(list, pass)

        break if new_num == number_of_root_threads
        number_of_root_threads = new_num
        pass += 1
      end
    end
  end
end
# vim: ts=2 sts=2 et ft=ruby:
