# Hotfixes for other classes

# If the INFO block contains multiple colons, @info will be wrong.
class Maildir::Message
  protected

  # Sets dir, unique_name, and info based on the key
  def parse_key(key)
    @dir, filename = key.split(File::SEPARATOR)
    @dir = @dir.to_sym
    @unique_name, @info = filename.split(COLON, 2)
  end
end
# vim: ts=2 sts=2 et ft=ruby:
